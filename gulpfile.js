// Declare dependancies
var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');
var notify = require('gulp-notify');
var autoprefixer = require('gulp-autoprefixer');

// Individual Tasks

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'working' // browserSync root folder
    },
    ghostMode: false,
    browser: ["chromium-browser", "firefox"],
    notify: false
  })
});

gulp.task('sass', function() {
  return gulp.src('working/sass/main.scss') // Get sass files
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], cascade: true
    }))
    .pipe(gulp.dest('working/css')) // Destination compiles css
    .pipe(browserSync.reload({ // reload browserSync
      stream: true
    }))
});

// Global Run Commands

gulp.task('default', ['browserSync', 'sass'], function() {
    gulp.watch('working/sass/**/*.scss', ['sass']);
    gulp.watch('working/js/**.js', browserSync.reload);
    gulp.watch('working/*.html', browserSync.reload);
});
