
<?php
        // Get the form fields from $_POST and sanitize them.
        $name = strip_tags(trim($_POST['name']));
		$name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_EMAIL);
        $message =  trim($_POST['message']);
        $message =  htmlspecialchars($message, ENT_COMPAT | ENT_HTML5 | ENT_QUOTES);

        // Set who the email is going to.
        $recipient = 'darryn@nyrrad.com';

        // Set the email subject.
        $subject = "Website contact from " . $name;

        // Build the email content.
        $email_content = "<div style=\"font-size: 16px;\"> <b>Name:</b> <br/>" . $name ."<br/><br/>"
        ."<b>Email:</b><br/>" . $email . "<br/><br/>"
        ."<b>Message:</b><br/>" . $message . "</div>";


        // Build the email headers.
        $email_headers = "MIME-Version: 1.0" . "\r\n";
        $email_headers .= "From: $name <$email>" . "\r\n";
        $email_headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        // Send the email.
        mail($recipient, $subject, $email_content, $email_headers);
?>
