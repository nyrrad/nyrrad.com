(function() {
    var mainNav = document.getElementById('main__nav'),
        mobileNav = document.getElementById('mobile__nav'),
        navButton = document.getElementById('mobile__button'),
        navLinks = document.getElementById('mobile__links'),
        address = window.location.href,
        currentState = '#top',
        previousState = null;

    function emailPost() {
      var request = new XMLHttpRequest(),
          form = document.getElementById('contact__form'),
          overlay = document.getElementById('contact__overlay'),
          checker = document.getElementById('checker'),
          post = [];

      // Check to see if the honey pot is checked
      if (checker.checked) {
        overlay.innerHTML = '<div class="outline__borders contact__overlay"><p class="alert"><span class="red">Buzz Buzz Buzz Bot Detected Buzz Buzz</span></p></div>';
        fadeOut(form, function() {
          fadeIn(overlay);
        });
      } else {
        // take the input values from the form and combined them into an array
        for (var i = 1; i < form.elements.length - 1; i++) {
          post.push(encodeURIComponent(form.elements[i].name) + '=' + (encodeURIComponent(form.elements[i].value)));
        }

        // Concat array values into a string. Seperating each value with a '&'
        // Completes taking values and making them 'application/x-www-form-urlencoded'
        // format
        post = post.join('&').replace(/%20/g, '+');

        // On submission fade out the form and fade in the thank you message
        fadeOut(form, function() {
          fadeIn(overlay);
        });

        // If there is an error append Thank you message to reflect the error
        request.onreadystatechange = function() {
          if (request.readyState === 4) {
            if (request.status !== 200) {
              overlay.innerHTML = '<div class="outline__borders contact__overlay"><p class="alert"><span class="red"> There was an error sending the message. Please try again later</span></p></div>';
            }
          }
        };

        // send the post string
        request.open('POST', 'php/qweldifghjwv.php');
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setRequestHeader('Content-Length', post.length);
        request.send(post);
      }
    }

    // Mobile menu functions. show and hide
    function hideMobile() {
      navButton.classList.remove('active');
      navLinks.style.marginTop = '0';
      navLinks.style.height = '0';
    }

    function showMobile() {
      navButton.classList.add('active');
      navLinks.style.marginTop = '1.25rem';
      navLinks.style.height = '6.75em';
    }

    // Fixed NAV show and content animations
    function lockNav() {
      var workSection = document.getElementById('work'),
          pageHeader = document.getElementById('top');

      if (window.pageYOffset > pageHeader.offsetHeight) {
        mainNav.style.position = 'fixed';
        mainNav.style.top = '0';
        mainNav.classList.add('no-max');
        workSection.style.marginTop = mainNav.offsetHeight.toString() + 'px';
      } else {
        mainNav.style.position = 'relative';
        mainNav.classList.remove('no-max');
        workSection.style.marginTop = '0px';
      }
    }

    // Fade out
    function fadeOut(n, callback) {
      n.style.opacity = '1';
      (function fade() {
        if ((n.style.opacity -= 0.1) === 0) {
          n.style.display = 'none';
          callback();
        } else {
          requestAnimationFrame(fade);
        }
      })();
    }

    // Fade in
    function fadeIn(n, display) {
      n.style.opacity = '0';
      n.style.display = display || 'block';
      (function fade() {
        var v = parseFloat(n.style.opacity);
        if ((v += 0.1) < 1) {
          n.style.opacity = v;
          requestAnimationFrame(fade);
        }
      })();
    }

    // // Scroll to
    // function scrollTo(element, duration) {
    //
    //     console.log(window.scrollY)
    //     console.log(document.documentElement.scrollTop);
    //
    //     e = (document.body || document.documentElement);
    //     var e = document.documentElement;
    //
    //     if( e.scrollTop === 0) {
    //       var t = e.scrollTop;
    //
    //       console.log(e.scrollTop);
    //       ++e.scrollTop;
    //
    //       console.log(++e.scrollTop);
    //       console.log(e.scrollTop--);
    //
    //       console.log(t + 1);
    //       if ( t + 1 === e.scrollTop--) {
    //         e = e;
    //       } else {
    //         e = document.body;
    //       }
    //     }
    //     scrollToC(e, window.scrollY, element, duration);
    //
    //     function scrollToC(element, from, to, duration) {
    //
    //       // if duration is 0, dont animate;
    //       if (duration <= 0) {
    //         return;
    //       }
    //
    //       if(typeof from === "object") {
    //         from = from.offsetTop;
    //       }
    //
    //       if(typeof to === "object") {
    //         if(!mainNav.offsetHeight !== 0) {
    //           hideMobile();
    //         }
    //         to = to.offsetTop + 1;
    //       }
    //
    //       // Find distance between elements.
    //       var distance =  (from - to) < 0 ? -(from - to) : (from - to);
    //
    //       // set duration based on distance to travel to avoid animation glitching
    //       // 1500 is a magic number solution... not that clean
    //
    //       duration = distance > 1500 ? duration * ((distance/1000)/3) : duration;
    //
    //       scrollToX(element, from, to, 0, 1/duration, 16, ease);
    //     }
    //
    //     function scrollToX(element, x1, x2, t, v, step, operacion) {
    //         if (t < 0 || t > 1 || v <= 0) {
    //           return;
    //         }
    //         element.scrollTop = x1 - (x1-x2)*operacion(t);
    //         t += v * step;
    //         setTimeout(function() {
    //             scrollToX(element, x1, x2, t, v, step, operacion);
    //         }, step);
    //     }
    //
    //     function ease(t){
    //       return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t;
    //     }
    // }

    function setAge() {
        var ageLocation = document.getElementById('age'),
            today = new Date(),
            birthday = new Date('06/19/1987'),
            age = (today.getFullYear() - birthday.getFullYear());

            if (today.getMonth() < birthday.getMonth() || today.getMonth() === birthday.getMonth() && today.getDate() < birthday.getDate()) {
                age--;
            }

            ageLocation.innerHTML = age;
    }

    function setYear() {
        var year = document.getElementById('year'),
            today = new Date();

            year.innerHTML = "2010 - " + today.getFullYear();
    }

    // Link event listeners
    document.addEventListener('click', function(event) {
      var target = event.target;

      // if (target.href) {
      // // if target is a #local link
      //     if (target.href.indexOf('#') > -1) {
      //       event.preventDefault();
      //
      //       var link = target.getAttribute('href'),
      //           linkHref = link.replace(/\u0023/, ''),
      //           location = document.getElementById(linkHref);
      //
      //       scrollTo(location, 300);
      //     }
      // }

      // if target is the mobile nav button
      if (target == navButton) {
        event.preventDefault();
        if (navButton.classList.contains('active')) {
          hideMobile();
        } else {
          showMobile();
        }
      }
    });

    // Window event listeners for non mobile menu being fixed after certain distance
    window.addEventListener('resize', lockNav);
    window.addEventListener('load', lockNav);
    window.addEventListener('scroll', function() {
        lockNav();

        var about = document.getElementById('about'),
            contact = document.getElementById('contact'),
            work = document.getElementById('work'),
            top = document.getElementById('top'),
            topHeight = top.offsetHeight,

            aboutOffset = Math.round(about.getBoundingClientRect().top) - 100,
            contactOffset = Math.round(contact.getBoundingClientRect().top) - 100,
            workOffset = Math.round(work.getBoundingClientRect().top) - 100,
            topOffset = Math.round(top.getBoundingClientRect().top);

        if (workOffset <= 0 && workOffset > -work.offsetHeight) {
            if (currentState !== '#work') {
                currentState = '#work';

                window.history.replaceState(currentState, null, address + currentState);
            }
        }

        if ((aboutOffset <= 0 && aboutOffset > -about.offsetHeight) && currentState !== '#about') {
            currentState = '#about';
            window.history.replaceState(currentState, null, address + currentState);
        }

        if ((contactOffset <= 0 && contactOffset > -contact.offsetHeight) && currentState !== '#contact') {
            currentState = '#contact';
            window.history.replaceState(currentState, null, address + currentState);
        }

        if ((topOffset + topHeight >= 0) && currentState !== '#top') {
            currentState = '#top';
            window.history.replaceState(currentState, null, address);
        }
    });

    // Form submit event listener
    document.getElementById('contact__form').addEventListener('submit', function(event) {
      event.preventDefault();
      emailPost();
    });

    // Set my age
    setAge();

    // set footer year
    setYear();

    //run svg pollyfill for ie-9-10-11 & chrome
    svg4everybody({polyfill: 'true'});
})();
